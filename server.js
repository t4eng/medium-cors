const express = require('express')
const axios = require('axios')
const path = require('path')
const app = express()

app.use(express.static(path.join(__dirname, 'build')))

app.post('/file/download', express.json())
app.post('/file/download', async (req, res) => {
    const { imageUrl } = req.body
    const result = await axios.get(imageUrl, { responseType: 'arraybuffer' })
    res.send(Buffer.from(result.data, 'binary').toString('base64'))
})

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'build', 'index.html'))
})

app.listen(3000, () => console.log("server running http://localhost:3000"))