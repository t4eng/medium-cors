import logo from './logo.svg';
import './App.css';
import axios from 'axios'

const contentType = "image/jpg"
const imageUrl = "https://weibuzz.files.wordpress.com/2018/10/iu.jpg"
const download = async () => {
  await axios.post("/file/download", { imageUrl }).then((response) => {
    const linkSource = `data:${contentType};base64,${response.data}`
    const downloadLink = document.createElement("a")
    downloadLink.href = linkSource
    downloadLink.download = "iu.jpg"
    downloadLink.click()
  })
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={imageUrl} alt="iu" />
        <button onClick={download} style={{ margin: "2rem", height: "2.5rem" }}>Download File</button>
      </header>
    </div>
  );
}

export default App;
